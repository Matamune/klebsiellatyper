### Authors & Funding

The *Klebsiella* implementation of this tool, called *Klebsiella* MALDI TyperR, was developed in the Brisse group.  
Website available at https://maldityper.pasteur.fr/.  
The original development of the MALDIquantTypeR tool was performed in the frame of the PhD research project of Sébastien Bridel, supervized by Eric Duchaud at INRAE, Pierre Yves Moalic at Labofarm and Sophie Pasek at MNHN.  
This website is inspired by the original website avalaible at http://genome.jouy.inra.fr/shiny/maldiquanttyper/. 

### People involved in the Klebsiella MALDI TypeR Project:


    Sylvain Brisse (Scientific manager, team leader, documentation)
    Sébastien Bridel (Initiator, coding, testing, documentation, evaluation)
    Carla Parada Rodrigues (MALDI-TOF spectra & analysis, biomarkers identification)

### People involved in the MALDIquantTypeR Project:


    Sébastien Bridel (1)
    Maroua Chadhil (5)
    Eric Duchaud (1)
    Jean-Francois Bernardet (1)
    Sophie Pasek (2)
    Arnaud Marie (3)
    Pierre-Yves Moalic (3)
    Jean Le Guennec (3)
    Klervi L'Her (4)
    Frédéric Bourgeon (4)
    Jean-Louis Pinsard (4)
    (1) Unité de Virologie et Immunologie Moléculaires, INRAE, Jouy en Josas, France.
    (2) Atelier de Bioinformatique, Muséum d'Histoire Naturelle, Paris, France.
    (3) Labofarm, Finalab, Loudéac, France. 
    (4) Bio Chêne Vert, Finalab, Châteaubourg, France
    (5) Student/trainee
    
### Publications


Sébastien Bridel, Stephen C. Watts, Louise M. Judd, Taylor Harshegyi, Virginie Passet, Carla Rodrigues, Kathryn E. Holt, Sylvain Brisse,
*Klebsiella* MALDI TypeR: a web-based tool for *Klebsiella* identification based on MALDI-TOF mass spectrometry,Research in Microbiology,Volume 172, Issues 4–5,2021,103835,ISSN 0923-2508

https://doi.org/10.1016/j.resmic.2021.103835.



Bridel, S., Bourgeon, F., Marie, A. et al. Genetic diversity and population structure of *Tenacibaculum maritimum*, a serious bacterial pathogen of marine fish: from genome comparisons to high throughput MALDI-TOF typing. Vet Res 51, 60 (2020).

https://doi.org/10.1186/s13567-020-00782-0
    
### Acknowledgements
    Stevenn Volant (R shiny tips)
    333 Design Studio (Klebsiella MALDI TypeR Logo)
    
### Funding
This work was financially supported by the MedVetKlebs project, a
component of European Joint Programme One Health EP, which
has received funding from the European Union’s Horizon 2020
Research and Innovation Programme under Grant Agreement
No. 773830.

The original MALDIquantTypeR projet was funded under the ANRT CIFRE grant number 2006/0707 and F2E “FishPathoMaldi” projects.

### Copyright & license


```
DESCRIPTION PROGRAM                                                  
Authors: Sebastien BRIDEL               
Copyright (c) 2021  Institut Pasteur
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as              
published by the Free Software Foundation.           
This program is distributed in the hope that it will be useful,          
but WITHOUT ANY WARRANTY; without even the implied warranty of           
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
GNU Affero General Public License for more details.
You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.       
```

The logo of Institut Pasteur is shared under license: Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0).  
No changes were made to the logo.  
https://creativecommons.org/licenses/by-sa/4.0/deed.en