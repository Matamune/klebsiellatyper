---
title: ""
output:
  html_document: default
  word_document: default
---
<style>
body {
text-align: justify}
</style>
<center>
![](www/LOGO SEBASTIEN BRIDEL 2-10.png){width=50%}
</center>
<br>
The purpose of this webapp tool is to identify a Klebsiella isolate from its MALDI-TOF mass spectrometry spectrum. This is applicable to members of the <i>Klebsiella pneumoniae</i> complex (<i>K. pneumoniae</i>, <i>K. variicola</i>, and phylogenetically related species) and of the <i>Klebsiella oxytoca</i> species complex (<i>Klebsiella oxytoca</i>, <i>K. pasteurii</i>, <i>K. spallanzanii</i> and phylogenetically related species).<br><br>

## Workflow overview
This tool requires uploading raw data obtained from a MALDI-TOF mass spectrometer. After pre-processing, detected peaks will be compared to reference peak lists. If the spectrum is first identified as a member of the <i>K. pneumoniae</i> or <i>K. oxytoca</i> species complexes, the tool will then apply the appropriate second step identification scheme to refine the identification at species (phylogroup) level, based on the combination of detected biomarkers. 

<center>
![](www/klebsiellashiny-schema2.png){width=40%}
</center>

## Disclaimer

<b>
Please be aware that this is a proof-of-concept website made for academic research purpose only.<br>
Do not use for medical diagnosis purposes.<br>
This software comes with absolutely no warranty or dedicated support.</b>