#################################################################################
# DESCRIPTION PROGRAM                                                           #
#                                                                               #
# Authors: Sebastien BRIDEL						                                          #
# Copyright (c) 2021  Institut Pasteur                                          #
#                                                                               #
#                                                                               #
# This program is free software: you can redistribute it and/or modify          #
# it under the terms of the GNU Affero General Public License as                #
# published by the Free Software Foundation.                                    #
#                                                                               #
# This program is distributed in the hope that it will be useful,               #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 #
# GNU Affero General Public License for more details.                           #
#                                                                               #
# You should have received a copy of the GNU Affero General Public License      #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.        #
#                                                                               #
#################################################################################
# Packages dependancies ==========
require(shinydashboard)
require(shinydashboardPlus)
require(shinyWidgets)
require(shinyBS)
require(readxl)
require(MALDIquantForeign)
require(MALDIquant)
require(MALDIrppa)
require(MassSpecWavelet)
require(dplyr)
require(shinycssloaders)
require(htmltools)
require(DT)
require(ggplot2)
require(reshape2)
require(plotly)
require(ggplot2)
require(reshape2)
require(plotly)
require(randomcoloR)
# Reference files ==========
complexIdentification <- read_excel("./refdata/complexIdentification.xlsx")
Kp_ref = read_excel("./refdata/Klebsiella_MALDI_peaks.xlsx",sheet = "Kpneumoniae complex")
Ko_ref = read_excel("refdata/Klebsiella_MALDI_peaks.xlsx", sheet = "Koxytoca complex")
Kp_biok_species_ref = Kp_ref$`m/z`[grep("complex",Kp_ref$Species)]
Ko_biok_species_ref = Ko_ref$`m/z`[grep("complex",Ko_ref$Species)]
Kp_refPeak = createMassPeaks(Kp_biok_species_ref,intensity = rep(1,length(Kp_biok_species_ref)))
Ko_refPeak = createMassPeaks(Ko_biok_species_ref,intensity = rep(1,length(Ko_biok_species_ref)))
Kp_MPST_scheme <- read_excel("./refdata/Kp-MPST-scheme.xlsx")
Ko_MPST_scheme <- read_excel("./refdata/Ko-MPST-scheme.xlsx")
Kp_MPST_phylogroups <- read_excel("./refdata/Kp-MPST-phylogroups.xlsx")
Ko_MPST_phylogroups <- read_excel("./refdata/Ko-MPST-phylogroups.xlsx")
refData.file <- read_excel("./refdata/refData.xlsx")
refData.avgspectra <- readRDS("./refdata/refAvgSpectra-oldR.rds")
refData.file = refData.file[order(match(refData.file$SAMPLE,names(refData.avgspectra))),]
refData.file$SAMPLE = c(paste("KoRef",refData.file$SAMPLE[1:18], sep =  "_"),paste("KpRef",refData.file$SAMPLE[19:34], sep =  "_"))
refData.colors <- read_excel("refdata/refColors.xlsx")
names(refData.avgspectra) = refData.file$SAMPLE
predefinedWindows <- read_excel("refdata/predefinedWindows.xlsx")
# Functions ==========
getSampleName = function(input,delimiter){
  t1 = strsplit(input, delimiter)
  new = c()
  for(idx in 1:length(t1)){
    baseName = paste(t1[[idx]][1:length(t1[[idx]])-1],collapse = "")
    new = c(new,baseName)
  }
  return(new)
}
myFuncTest1 = function(data,customSNR){
    thScale <- 2
    ite <- 105
    SigNoi <- 2.5
    hws <- 20
    tol <- 2e-3
    # spectra = transfIntensity(data, fun = sqrt)
    # spectra = wavSmoothing(spectra, thresh.scale = thScale)
    # spectra = removeBaseline(spectra, method = "SNIP", iterations = ite)
    # spectra = calibrateIntensity(spectra, method = "PQN")
    # spectra2 = alignSpectra(spectra,halfWindowSize = hws,SNR = SigNoi, tolerance = tol, noiseMethod = "MAD",
    #                         warpingMethod = "lowess",reference = Ko_refPeak)
    
    spectra <- transformIntensity(data, method="sqrt")
    spectra <- smoothIntensity(spectra, method='SavitzkyGolay')
    spectra <- removeBaseline(spectra, method='SNIP')
    spectra <- calibrateIntensity(spectra, method='TIC')
    spectra2 <- alignSpectra(spectra, reference = Ko_refPeak, allowNoMatches=TRUE)
    
    samples <- sapply(spectra2,function(x)metaData(x)$sampleName)
    samples <- gsub("_[1-9][A-Z]$","",samples)
    
    avgSpectra = averageMassSpectra(spectra2,labels = samples,method = "mean")
    mass_1 = avgSpectra[[1]]@mass
    inty_1 = avgSpectra[[1]]@intensity
    name_1 = avgSpectra[[1]]@metaData$sampleName
    print(paste("[",1,"] ",name_1,sep = ''))
    peakInfo = peakDetectionCWT(inty_1, SNR.Th = customSNR, nearbyPeak = T)
    majorPeakInfo = peakInfo$majorPeakInfo
    betterPeakInfo = tuneInPeakInfo(inty_1, majorPeakInfo)
    finalpeak_vector = mass_1[betterPeakInfo$peakIndex]
    finalint_vector = inty_1[betterPeakInfo$peakIndex]
    # ====
    ppm = 6e2/1e6
    res = NULL
    cres = NULL
    cressMass = NULL
    for(biomarqueur in unique(Ko_MPST_scheme$protein)){
      subdata = Ko_MPST_scheme[Ko_MPST_scheme$protein == biomarqueur,]
      x = finalpeak_vector
      sorted.x = sort(unlist(x))
      y =  subdata$mass_theo
      mat = abs(outer(sorted.x,y,`-`))
      min_diff_value = min(abs(mat))
      dim = which(mat==min_diff_value, arr.ind = T)
      min_val_query = sorted.x[dim[,1]]
      min_val_ref = y[dim[,2]]
      if(min_diff_value <= min_val_query * ppm){
        IF = subdata$fullname[dim[,2]]
        IF = gsub(pattern = "^(.)+?IF",replacement = "",x = IF)
        cres = c(cres,IF)
        cval = round(min_val_query,3)
        cressMass = c(cressMass,cval)
      }else{
        cres = c(cres ,"NT")
        cressMass = c(cressMass,0)
      }
    }
    res = rbind(res,c("Detected peaks",cressMass))
    res = rbind(res,c("Corresponding isoforms",cres))
    colnames(res) = c("  ", unique(Ko_MPST_scheme$protein))
    res[res=="NT"] = 0
    res2 = res[,-4]
    #=====
    for(idx in 1:nrow(res2)){
      malditype = "not identified"
      phylogroup = "not identified"
      species = "not identified"
      complementarySp = "not identified"
      complementaryPhylo = "not identified"
      
      line1 = paste(res2[idx,2:6],collapse = "_")
      linecomp1 = paste(res2[idx,7:9],collapse = "_")
      for(i in 1:nrow(Ko_MPST_phylogroups)){
        line2 = paste(Ko_MPST_phylogroups[i,c(4,5,7,8,9)],collapse = "_")
        if (line1 == line2){
          species = as.character(Ko_MPST_phylogroups[i,1])
          phylogroup = as.character(Ko_MPST_phylogroups[i,2])
          malditype = as.character(Ko_MPST_phylogroups[i,3])
        }
      }      
      for(i in 1:nrow(Ko_MPST_phylogroups)){
        linecomp2 = paste(Ko_MPST_phylogroups[i,c(10,11,12)],collapse = "_")
        if (linecomp1 == linecomp2){
          if(malditype == "MT1"){
            complementaryPhylo = as.character(Ko_MPST_phylogroups[i,2])
            complementarySp = as.character(Ko_MPST_phylogroups[i,1])
          }else{
            complementaryPhylo = phylogroup
            complementarySp = species
          }
        }
      }
    }
    res2 = cbind(sample = samples[1],res2)
    res2 = cbind(res2,phylogroup,species,malditype,complementaryPhylo,complementarySp)
    RES = list()
    RES[[1]] = avgSpectra
    RES[[2]] = cbind.data.frame(samples[1],finalpeak_vector,finalint_vector)
    RES[[3]] = res2
    return(RES)
}
myFuncTest2 = function(data,customSNR){
  print("DOSOMETHING KP")
  thScale = 2
  ite = 105
  SigNoi = 2.5
  hws = 20
  tol = 2e-3
  # spectra = transfIntensity(data, fun = sqrt)
  # spectra = wavSmoothing(spectra, thresh.scale = thScale)
  # spectra = removeBaseline(spectra, method = "SNIP", iterations = 50)
  # spectra = calibrateIntensity(spectra, method = "PQN")
  # spectra2 = alignSpectra(spectra,halfWindowSize = hws,SNR = SigNoi, tolerance = tol, noiseMethod = "MAD",
  #                         warpingMethod = "lowess",reference = Kp_refPeak)
  
  spectra <- transformIntensity(data, method="sqrt")
  spectra <- smoothIntensity(spectra, method='SavitzkyGolay')
  spectra <- removeBaseline(spectra, method='SNIP')
  spectra <- calibrateIntensity(spectra, method='TIC')
  spectra2 <- alignSpectra(spectra, reference = Kp_refPeak, allowNoMatches=TRUE)
  
  samples <- sapply(spectra2,function(x)metaData(x)$sampleName)
  samples <- gsub("_[1-9][A-Z]$","",samples)
  avgSpectra = averageMassSpectra(spectra2,labels = samples,method = "mean")
  print("FINAL-avgmassspectra DONE")
  mass_1 = avgSpectra[[1]]@mass
  inty_1 = avgSpectra[[1]]@intensity
  name_1 = avgSpectra[[1]]@metaData$sampleName
  print(paste("[",1,"] ",name_1,sep = ''))
  peakInfo = peakDetectionCWT(inty_1, SNR.Th = customSNR, nearbyPeak = T)
  majorPeakInfo = peakInfo$majorPeakInfo
  betterPeakInfo = tuneInPeakInfo(inty_1, majorPeakInfo)
  raw_spectra_peaks = mass_1[betterPeakInfo$peakIndex]
  raw_spectra_inty = betterPeakInfo$peakValue
  raw_spectra_scale = betterPeakInfo$peakScale
  raw_spectra.meta = name_1
  finalpeak_vector = mass_1[betterPeakInfo$peakIndex]
  finalint_vector = inty_1[betterPeakInfo$peakIndex]
  #======
  ppm = 5e2/1e6
  cres = NULL
  cressMass = NULL
  res = NULL
  for(biomarqueur in unique(Kp_MPST_scheme$protein)){
    subdata = Kp_MPST_scheme[Kp_MPST_scheme$protein == biomarqueur,]
    x = unlist(raw_spectra_peaks)
    sorted.x = sort(x)
    y =  subdata$mass_theo
    mat = abs(outer(sorted.x,y,`-`))
    min_diff_value = min(abs(mat))
    dim = which(mat==min_diff_value, arr.ind = T)
    min_val_query = sorted.x[dim[,1]]
    min_val_ref = y[dim[,2]]
    if(min_diff_value <= min_val_query * ppm){
      IF = subdata$fullname[dim[,2]]
      IF = gsub(pattern = "^(.)+?IF",replacement = "",x = IF)
      cres = c(cres,IF)
      cval = round(min_val_query,3)
      cressMass = c(cressMass,cval)
    }else{
      cres = c(cres ,"NT")
      cressMass = c(cressMass,0)
    }
  }
  print("res done")
  res = rbind(res,c("Detected peaks",cressMass))
  res = rbind(res,c("Corresponding isoforms",cres))
  colnames(res) = c("  ", unique(Kp_MPST_scheme$protein))
  res2 = res[,-8]
  #=====
  for(idx in 1:nrow(res2)){
    phylogroup = "not identified"
    species = "not identified"
    line1 = paste(res2[idx,-1],collapse = "_")
    for(i in 1:nrow(Kp_MPST_phylogroups)){
      line2 = paste(Kp_MPST_phylogroups[i,3:ncol(Kp_MPST_phylogroups)],collapse = "_")
      if (line1 == line2){
        species = as.character(Kp_MPST_phylogroups[i,1])
        print(species)
        phylogroup = as.character(Kp_MPST_phylogroups[i,2])
      }
    }
  }
  res2 = cbind(sample = samples[1],res2)
  res2 = cbind(res2,phylogroup,species)
  RES = list()
  RES[[1]] = avgSpectra
  RES[[2]] = cbind.data.frame(samples[1],finalpeak_vector,finalint_vector)
  RES[[3]] = res2
  return(RES)
}